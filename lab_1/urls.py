from django.urls import re_path
from .views import index, form, kontak, hobby
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^form/', form, name='form'),
    re_path(r'^hobby/', hobby, name='hobby'),
    re_path(r'^kontak/', kontak, name='kontak'),
]
